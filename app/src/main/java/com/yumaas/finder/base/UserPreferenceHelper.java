package com.yumaas.finder.base;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import com.yumaas.finder.base.volleyutils.MyApplication;
import com.yumaas.finder.login.UserItem;



public class UserPreferenceHelper {

    private static SharedPreferences sharedPreferences = null;

    //here you can find shared preference operations like get saved data for user


    private UserPreferenceHelper() {

    }

    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences != null) return sharedPreferences;
        return sharedPreferences = context.getSharedPreferences("savedData", Context.MODE_PRIVATE);
    }

    public static int getUserId() {
        return getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getInt("userId", -1);
    }

    public static void setUserId(int userId) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        prefsEditor.putInt("userId", userId);
        prefsEditor.commit();
    }


    public static void setRememberMe(boolean rememberMe) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        prefsEditor.putBoolean("rememberMe", rememberMe);
        prefsEditor.commit();
    }

    public static boolean isRemember() {
        return getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getBoolean("rememberMe", false);
    }

    public static void saveUserDetails(UserItem userModel) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        prefsEditor.putInt("userId", userModel.getId());
        prefsEditor.commit();
    }

    public static void clearUserDetails() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
        prefsEditor.putString("userDetails", null);
        prefsEditor.putInt("userId", 0);
        prefsEditor.commit();
    }

    public static UserItem getUserDetails() {
        Context context = MyApplication.getInstance().getApplicationContext();
        Gson gson = new Gson();
        String json = getSharedPreferenceInstance(context).getString("userDetails", "");
        if (json == null || json.equals("")) return new UserItem();
        return gson.fromJson(json, UserItem.class);
    }


    public static boolean isLogined() {
        boolean isLogined = (getUserDetails() != null) && getUserDetails().getId() != 0;
        return isLogined;
    }

    public static boolean isLogined(Context context) {
        boolean isLogin = isLogined();
        if (!isLogin) {
//            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
//                    .setTitleText(ResourcesManager.getString(R.string.label_you_not_login))
//                    .setContentText(ResourcesManager.getString(R.string.label_login_first))
//                    .setConfirmText(ResourcesManager.getString(R.string.login))
//                    .setConfirmClickListener(sDialog -> {
//                        MovementManager.startActivity(context, Codes.LOGIN_SCREEN);
//                        sDialog.dismissWithAnimation();
//                    })
//                    .setCancelButton(ResourcesManager.getString(R.string.action_cancel),
//                            sDialog -> sDialog.dismissWithAnimation())
//                    .show();

        }
        return isLogin;
    }


    public static void setLanguage(String language) {
        SharedPreferences userDetails = MyApplication.getInstance().getApplicationContext()
                .getSharedPreferences("languageData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString("language", language);
        editor.putBoolean("haveLanguage", true);
        editor.commit();
    }

    public static String getCurrentLanguage() {
        SharedPreferences sharedPreferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences("languageData", Context.MODE_PRIVATE);
        if (!sharedPreferences.getBoolean("haveLanguage", false)) return "en";
        return sharedPreferences.getString("language", "en");
    }

    public static boolean isFirstTime(Context context) {
        boolean isFirstTime = getSharedPreferenceInstance(context).getBoolean("firstTime", true);
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(context).edit();
        prefsEditor.putBoolean("firstTime", false);
        prefsEditor.commit();

        return isFirstTime;
    }


    public static void saveGoogleToken(String token) {
        if (token == null || token.equals("")) {
            return;
        } else {
            SharedPreferences.Editor editor = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).edit();
            editor.putString("googleToken", token);
            editor.commit();
        }
    }

    public static String getGoogleToken() {
        String token = getSharedPreferenceInstance(MyApplication.getInstance().getApplicationContext()).getString("googleToken", "") + "";
        return (token.length() < 1 || token == null) ? "EmptyToken" : token;
    }


    public static void savePassword(String pass) {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication
                .getInstance().getApplicationContext()).edit();
        String password = pass;
        prefsEditor.putString("userPassword", password);
        prefsEditor.commit();
    }

    public static void clearPassword() {
        SharedPreferences.Editor prefsEditor = getSharedPreferenceInstance(MyApplication
                .getInstance().getApplicationContext()).edit();
        prefsEditor.putString("userPassword", null);
        prefsEditor.commit();
    }

    public static String getPassword() {
        Context context = MyApplication.getInstance().getApplicationContext();
        String password = getSharedPreferenceInstance(context).getString("userPassword", "");
        return password;
    }


}
