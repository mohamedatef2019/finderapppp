package com.yumaas.finder.noneed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.finder.R;


public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.ViewHolder> {

    Context context;
   String []images={"https://mashroo3k.com/new/wp-content/uploads/2017/03/Western-Union-logo-WU-1024x762.png"
   ,"https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-thumbnail/s3/0025/1237/brand.gif?itok=ECTyqve3"
   ,"https://thumbs.dreamstime.com/b/money-cash-logo-vector-green-91037524.jpg"
   ,"https://static.vecteezy.com/system/resources/thumbnails/000/619/566/small/campr-15.jpg"
   ,"https://i.pinimg.com/originals/bc/c2/85/bcc2856a837b3894332efece359278c7.jpg"
   ,"https://thumbs.dreamstime.com/b/money-logo-template-design-vector-icon-illustration-money-logo-template-design-vector-icon-illustration-163187565.jpg"
   ,"https://thumbs.dreamstime.com/b/king-money-icon-logo-design-element-can-be-used-as-as-complement-to-96618155.jpg"
   ,"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTpXIN47KdUd5xfusk_PA9robpLIlUQ2lFskhD_C-LS8kJvg453Ut8ksDwkHte6qNdo3g&usqp=CAU"};

    String []names={"Wester Union"
    ,"Cloud Change"
    ,"Master Amazing"
    ,"Egypt Change"
    ,"Ready"
    ,"Yumaas"
    ,"Green Cut"
    ,"Orange Money"};

    String []details={"10 USD"
            ,"5 %"
            ,"5 % + 1 USD"
            ,"Free"
            ,"2 %"
            ,"3 %"
            ,"4 %"
            ,"2 %"};


    public CurrencyAdapter(Context context) {
        this.context = context;
    }


    @Override
    public CurrencyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_branch, parent, false);
        CurrencyAdapter.ViewHolder viewHolder = new CurrencyAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CurrencyAdapter.ViewHolder holder, final int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return names.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View itemView) {
            super(itemView);


        }
    }
}
