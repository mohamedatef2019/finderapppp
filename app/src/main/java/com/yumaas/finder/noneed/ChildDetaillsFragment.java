package com.yumaas.finder.noneed;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.yumaas.finder.R;


public class ChildDetaillsFragment extends Fragment {

    View rootView;
    SliderView sliderView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fagment_child_details, container, false);





        setSliderView();
        return rootView;
    }

    private void setSliderView( ){

        sliderView =  rootView.findViewById(R.id.imageSlider);
        //sliderView.setSliderAdapter(new SliderAdapterBarber(getActivity()));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.FANTRANSFORMATION);
        sliderView.setScrollTimeInSec(4);
    }
}