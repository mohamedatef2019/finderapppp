package com.yumaas.finder.noneed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.finder.R;
import com.yumaas.finder.posts.response.PostsItem;

import java.util.ArrayList;
import java.util.List;


public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    Context context;
    List<PostsItem> postsItems;


    public PostsAdapter(Context context,List<PostsItem>postsItems) {
        this.postsItems=postsItems;
        this.context = context;
    }


    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        PostsAdapter.ViewHolder viewHolder = new PostsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PostsAdapter.ViewHolder holder, final int position) {

        holder.name.setText(postsItems.get(position).getUser().getName());

        holder.details.setText(
                postsItems.get(position).getPhone()+"\n"
                +postsItems.get(position).getDetails());

        try {
            Picasso.get().load(postsItems.get(position).getImages().get(0).getImage()).into(holder.image);
        }catch (Exception e){
            e.getStackTrace();
        }
        holder.itemView.setOnClickListener(view -> {

          //  FragmentHelper.replaceFragment(view.getContext(), new ChildDetaillsFragment(), "CompanyDetailsFragment");


        });


    }

    @Override
    public int getItemCount() {
        return postsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);

        }
    }
}
