package com.yumaas.finder.noneed;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.yumaas.finder.BuildConfig;
import com.yumaas.finder.R;
import com.yumaas.finder.aboutus.AboutUsFragment;
import com.yumaas.finder.home.HomeFragment;
import com.yumaas.finder.posts.PostsFragment;
import com.yumaas.finder.updateprofile.UpdateProfileFragment;

import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    SmoothBottomBar smoothBottomBar;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
        smoothBottomBar = findViewById(R.id.bottomBar);

        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {

                FragmentHelper.popAllFragments(MainActivity.this);
                if(i==0){
                    FragmentHelper.replaceFragment(MainActivity.this, new HomeFragment(), "HomeFragment");
                }else if(i==1){
                    FragmentHelper.replaceFragment(MainActivity.this, new PostsFragment(), "CompaniesFragment");
                }else {
                    FragmentHelper.replaceFragment(MainActivity.this, new UpdateProfileFragment(), "ProfileFragment");

                }
                return false;
            }
        });

        findViewById(R.id.notifications).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(MainActivity.this,new NotficationsFragment(),"NotficationsFragment");

            }
        });

        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},342);

        navigationView.setNavigationItemSelectedListener(this);


        FragmentHelper.addFragment(this,new HomeFragment(),"HomeFragment");
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

            drawer.closeDrawer(GravityCompat.START);

        if (id != R.id.nav_share && id != R.id.nav_log_out && id != R.id.nav_rate) {

            FragmentHelper.popAllFragments(this);
        }

          if (id == R.id.nav_share) {

            Intent
                    sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey check out my app at: https://play.google.com/store/apps/details?id="+ BuildConfig.APPLICATION_ID);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if(id==R.id.nav_rate){
              final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
              try {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
              } catch (android.content.ActivityNotFoundException anfe) {
                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
              }
          }

          else if (id == R.id.nav_home) {
            FragmentHelper.replaceFragment(this, new HomeFragment(), "HomeFragment");
        }  else if (id == R.id.nav_about) {
              FragmentHelper.replaceFragment(this, new AboutUsFragment(), "AboutUsFragment");
          }
        else if (id == R.id.nav_log_out) {
           finish();
        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
