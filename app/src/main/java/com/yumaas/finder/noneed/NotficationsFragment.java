package com.yumaas.finder.noneed;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.finder.R;


public class NotficationsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);



        final NotificationsAdapter teachersAdapter = new NotificationsAdapter(getActivity());


        final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
        programsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        programsList.setAdapter(teachersAdapter);


        return rootView;
    }
}