package com.yumaas.finder.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserItem{

	@Expose
	@SerializedName("password")
	private String password;

	@Expose
	@SerializedName("google_id")
	private String googleId;

	@Expose
	@SerializedName("phone")
	private String phone;

	@Expose
	@SerializedName("name")
	private String name;

	@Expose
	@SerializedName("created_at")
	private String createdAt;

	@Expose
	@SerializedName("id")
	private int id;

	@Expose
	@SerializedName("email")
	private String email;

	@Expose
	@SerializedName("image")
	private String image;


	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setGoogleId(String googleId){
		this.googleId = googleId;
	}

	public String getGoogleId(){
		return googleId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImage() {
		return image;
	}

	@Override
 	public String toString(){
		return 
			"UserItem{" + 
			"password = '" + password + '\'' + 
			",google_id = '" + googleId + '\'' + 
			",phone = '" + phone + '\'' + 
			",name = '" + name + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}