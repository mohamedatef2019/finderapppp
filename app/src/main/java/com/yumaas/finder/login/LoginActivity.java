package com.yumaas.finder.login;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.finder.R;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.Validate;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.noneed.MainActivity;
import com.yumaas.finder.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity {
    EditText email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email=findViewById(R.id.email);
        password=findViewById(R.id.password);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 342);
        }
        clickListeners();

    }

    private void clickListeners(){
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Validate.isEmpty(email.getText().toString())||Validate.isEmpty(password.getText().toString())){
                    Toast.makeText(LoginActivity.this, "please fill all fields", Toast.LENGTH_SHORT).show();
                }else {
                    loginApi();
                }
            }
        });


        findViewById(R.id.rl_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }


    public void loginApi(){

        LoginRequest loginRequest = new LoginRequest("login");
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                UserResponse userResponse = (UserResponse)response;
                if (userResponse.getState()==101){
                    userResponse.getUser().setPassword(password.getText()+"");
                    UserPreferenceHelper.saveUserDetails(userResponse.getUser());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }else {
                    Toast.makeText(LoginActivity.this, "Sorry wrong password", Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserResponse.class);
    }
}