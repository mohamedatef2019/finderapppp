package com.yumaas.finder.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.finder.R;
import com.yumaas.finder.home.response.ChildsItem;
import com.yumaas.finder.noneed.ChildDetaillsFragment;
import com.yumaas.finder.noneed.FragmentHelper;
import com.yumaas.finder.userdetails.UserDetailsFragment;

import java.util.List;


public class ChildsAdapter extends RecyclerView.Adapter<ChildsAdapter.ViewHolder> {

    Context context;
    List<ChildsItem>childsItems;
    public ChildsAdapter(Context context,List<ChildsItem>childsItems) {
        this.context = context;
        this.childsItems=childsItems;
    }


    @Override
    public ChildsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child, parent, false);
        ChildsAdapter.ViewHolder viewHolder = new ChildsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChildsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(childsItems.get(position).getName());
        holder.details.setText(childsItems.get(position).getPhone());

        Picasso.get().load(childsItems.get(position).getImage()).into(holder.image);

        holder.itemView.setOnClickListener(view -> {
            UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("code",childsItems.get(position).getCode());
            userDetailsFragment.setArguments(bundle);
            FragmentHelper.replaceFragment(view.getContext(), userDetailsFragment, "CompanyDetailsFragment");
        });


    }

    @Override
    public int getItemCount() {
        return childsItems==null?0:childsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);

        }
    }
}
