package com.yumaas.finder.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.yumaas.finder.base.DefaultRequest;


public class RegisterRequest extends DefaultRequest {


    @Expose
    @SerializedName("user_id")
    private String userId;



    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("password")
    private String password;



    public RegisterRequest(String method) {
        super(method);
    }


    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }

    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
