package com.yumaas.finder.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.finder.R;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.Validate;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.login.LoginActivity;
import com.yumaas.finder.login.LoginRequest;
import com.yumaas.finder.login.UserResponse;
import com.yumaas.finder.noneed.MainActivity;

public class RegisterActivity extends AppCompatActivity {
    EditText name,phone,email,password;
    RelativeLayout returnLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        returnLayout=findViewById(R.id.return_layout);
        email=findViewById(R.id.email);
        password=findViewById(R.id.password);
        name=findViewById(R.id.name);
        phone=findViewById(R.id.phone);

        clickListeners();

    }

    private void clickListeners(){

        returnLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.register).setOnClickListener(view -> {
            if(Validate.isEmpty(email.getText().toString())||Validate.isEmpty(password.getText().toString())){
                Toast.makeText(RegisterActivity.this, "please fill all fields", Toast.LENGTH_SHORT).show();
            }else {
                registerApi();
            }
        });
    }


    public void registerApi(){

        RegisterRequest loginRequest = new RegisterRequest("register");
        loginRequest.setEmail(email.getText().toString());
        loginRequest.setPassword(password.getText().toString());
        loginRequest.setName(name.getText().toString());
        loginRequest.setPhone(phone.getText().toString());

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                UserResponse userResponse = (UserResponse)response;
                if (userResponse.getState()==101){
                    UserPreferenceHelper.saveUserDetails(userResponse.getUser());
                    startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                }else {
                    Toast.makeText(RegisterActivity.this, "Sorry  this phone or email used before", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, UserResponse.class);
    }


}