package com.yumaas.finder.posts;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.yumaas.finder.R;
import com.yumaas.finder.base.DefaultResponse;
import com.yumaas.finder.base.ResourcesManager;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.constantsutils.Codes;
import com.yumaas.finder.base.filesutils.FileOperations;
import com.yumaas.finder.base.filesutils.VolleyFileObject;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.home.HomeFragment;
import com.yumaas.finder.noneed.FragmentHelper;
import com.yumaas.finder.noneed.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;


public class AddSocialPostFragment extends Fragment {

    View rootView;
    EditText addImages, etAddPostDetails, phone;
    Button btn;
    int i = 1;

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_new_post, container, false);
        addImages = rootView.findViewById(R.id.add_images);
        etAddPostDetails = rootView.findViewById(R.id.etAddPostDetails);
        btn = rootView.findViewById(R.id.btn);

        phone = rootView.findViewById(R.id.phone);

        volleyFileObjects = new ArrayList<>();
        requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 124);

        addImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[]choiceString = new String[]{"Gallery", "Camera"};
                AlertDialog.Builder dialog = new AlertDialog.Builder(requireActivity());
                dialog.setIcon(R.mipmap.ic_launcher);
                dialog.setTitle("Select from");
                dialog.setItems(choiceString,
                        (dialog1, which) -> {
                            Intent intent;
                            if (which == 0) {
                                intent = new Intent(
                                        Intent.ACTION_PICK,
                                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            } else {
                                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            }
                         startActivityForResult(Intent.createChooser(intent, "Select profile picture"), Codes.FILE_TYPE_IMAGE);
                        }).show();
            }
        });

        btn.setOnClickListener(view -> addPost());

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            VolleyFileObject volleyFileObject =
                    FileOperations.getVolleyFileObject(getActivity(), data, "image_" + i,
                            Codes.FILE_TYPE_IMAGE);
            volleyFileObjects.add(volleyFileObject);
            addImages.setText("(" + i + ") Selected");

            i++;
        } catch (Exception e) {
            e.getStackTrace();
        }
    }


    public void addPost() {

        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("method", "add_post");
        hashMap.put("user_id", "" + UserPreferenceHelper.getUserDetails().getId());
        hashMap.put("details", etAddPostDetails.getText().toString());
        hashMap.put("phone", phone.getText().toString());


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                Toast.makeText(getActivity(), "Added successfully", Toast.LENGTH_SHORT).show();
                FragmentHelper.popAllFragments(requireActivity());

                    FragmentHelper.replaceFragment(requireActivity(), new PostsFragment(), "CompaniesFragment");


            }

            @Override
            public void onRequestError(Object error) {
            }
        }).multiPartConnect(hashMap, volleyFileObjects, DefaultResponse.class);
    }
}