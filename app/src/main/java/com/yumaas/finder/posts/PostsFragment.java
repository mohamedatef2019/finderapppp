package com.yumaas.finder.posts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.finder.R;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.home.UsersRequest;
import com.yumaas.finder.noneed.FragmentHelper;
import com.yumaas.finder.noneed.PostsAdapter;
import com.yumaas.finder.posts.response.PostsResponse;


public class PostsFragment extends Fragment {

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list, container, false);

        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new AddSocialPostFragment(),"AddSocialPostFragment");
            }
        });

        getPosts();




        return rootView;
    }

    public void getPosts(){

        UsersRequest loginRequest = new UsersRequest("posts");
        loginRequest.setOwnerId(UserPreferenceHelper.getUserDetails().getId()+"");

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {

                PostsResponse postsResponse = (PostsResponse)response;
                final PostsAdapter teachersAdapter = new PostsAdapter(getActivity(),postsResponse.getPosts());
                final RecyclerView programsList = rootView.findViewById(R.id.recycler_view);
                programsList.setLayoutManager(new LinearLayoutManager(getActivity()));
                programsList.setAdapter(teachersAdapter);

            }

            @Override
            public void onRequestError(Object error) {

            }
        }).requestJsonObject(loginRequest, PostsResponse.class);
    }
}