package com.yumaas.finder.updateprofile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;


import com.yumaas.finder.R;
import com.yumaas.finder.base.BaseFragment;
import com.yumaas.finder.base.ResourcesManager;
import com.yumaas.finder.base.UserPreferenceHelper;
import com.yumaas.finder.base.Validate;
import com.yumaas.finder.base.constantsutils.Codes;
import com.yumaas.finder.base.volleyutils.ConnectionHelper;
import com.yumaas.finder.base.volleyutils.ConnectionListener;
import com.yumaas.finder.databinding.FragmentProfileBinding;
import com.yumaas.finder.login.UserItem;
import com.yumaas.finder.login.UserResponse;
import com.yumaas.finder.register.RegisterRequest;


public class UpdateProfileFragment extends BaseFragment {

    private FragmentProfileBinding fragmentRegisterBinding;
    private View rootView;
    private UserItem userItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        init();
        fragmentRegisterBinding.btnRegisterEnter.setText("Update");
        setData();
        clickListeners();
        return rootView;
    }


    private void init() {
        rootView = fragmentRegisterBinding.getRoot();
    }

    private void clickListeners() {
        fragmentRegisterBinding.btnRegisterEnter.setOnClickListener(view -> {
            if (validate()) {
                registerApi();
            }
        });
    }


    private boolean validate() {
        if (Validate.isEmpty(fragmentRegisterBinding.etRegisterPhone.getText().toString())) {
            fragmentRegisterBinding.etRegisterPhone.setError(ResourcesManager.getString(R.string.label_empty));
            return false;
        }   else if (Validate.isEmpty(fragmentRegisterBinding.etRegisterPassword.getText().toString())) {
            fragmentRegisterBinding.etRegisterPassword.setError(ResourcesManager.getString(R.string.label_empty));
            return false;
        } else if (Validate.isEmpty(fragmentRegisterBinding.etRegisterName.getText().toString())) {
            fragmentRegisterBinding.etRegisterName.setError(ResourcesManager.getString(R.string.label_empty));
            return false;
        }

        return true;
    }


    private void setData() {

        userItem = UserPreferenceHelper.getUserDetails();

        fragmentRegisterBinding.etRegisterPhone.setEnabled(false);

        fragmentRegisterBinding.etRegisterPhone.setText(userItem.getPhone());
        fragmentRegisterBinding.etRegisterName.setText(userItem.getName());
        fragmentRegisterBinding.etRegisterPassword.setText(userItem.getPassword());



    }

    public void registerApi() {
        accessLoadingBar(View.VISIBLE);

        RegisterRequest registerRequest = new RegisterRequest("update_profile");
        registerRequest.setMethod("update_profile");
        registerRequest.setUserId(userItem.getId()+"");
        registerRequest.setPhone(fragmentRegisterBinding.etRegisterPhone.getText().toString());
        registerRequest.setName(fragmentRegisterBinding.etRegisterName.getText().toString());
        registerRequest.setPassword(fragmentRegisterBinding.etRegisterPassword.getText().toString());


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                accessLoadingBar(View.GONE);
                UserResponse userResponse = (UserResponse) response;
                if (userResponse.getState() == 101) {
                    Toast.makeText(getActivity(), "Updated ", Toast.LENGTH_SHORT).show();
                    UserItem savedUser = userResponse.getUser();
                    savedUser.setPassword(fragmentRegisterBinding.etRegisterPassword.getText()+"");
                    UserPreferenceHelper.saveUserDetails(savedUser);


                }
            }

            @Override
            public void onRequestError(Object error) {
                accessLoadingBar(View.GONE);
            }
        }).requestJsonObject(registerRequest, UserResponse.class);
    }


}
